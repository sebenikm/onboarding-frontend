import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { tap } from 'rxjs/operators';

import { environment } from './../environments/environment';
import { UserService } from './user.service';
import { ToasterService } from './services/toaster.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    constructor(private _userService: UserService, private _toasterService: ToasterService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const user = this._userService.user;
        if (user && req.url.startsWith(environment.apiUrl)) {
            req = req.clone({
                headers: req.headers.set(
                    'Authorization', `Bearer ${user.token}`
                )
            });
        }
        return next.handle(req).pipe(
            tap(
                () => {},
                error => console.log('ERROR', error)
            )
        );
    }

}
