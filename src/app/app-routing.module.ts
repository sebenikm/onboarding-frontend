import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { AdminGuard } from './admin.guard';


const routes: Routes = [
  { path: 'locations', loadChildren: () => import('./locations/locations.module').then(m => m.LocationsModule), canLoad: [AuthGuard] },
  { path: 'companies', loadChildren: () => import('./companies/companies.module').then(m => m.CompaniesModule), canLoad: [AuthGuard] },
  { path: 'events', loadChildren: () => import('./events/events.module').then(m => m.EventsModule), canLoad: [AuthGuard] },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule), canLoad: [AuthGuard] },
  { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule), canLoad: [AuthGuard, AdminGuard] },
  { path: '**', redirectTo: ''},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
