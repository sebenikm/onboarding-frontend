import { AuthEffects } from './store/effects/auth.effects';
import { reducers } from './store/reducers/index';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AuthInterceptor } from './auth.interceptor';
import { AppComponent } from './app.component';
import { ApiInterceptor } from './api.interceptor';
import { HeaderComponent } from './header/header.component';
import { LocationsEffects, EventsEffects, CompaniesEffects } from './store/effects';
import { UsersEffects } from './store/effects/users.effects';
import { ToasterComponent } from './components/toaster/toaster.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ToasterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([LocationsEffects, EventsEffects, CompaniesEffects, AuthEffects, UsersEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  entryComponents: [ToasterComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
