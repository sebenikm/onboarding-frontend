import { createAction, props } from '@ngrx/store';
import { IUser } from 'src/app/user.model';

const prefix = '[AUTH]';

export const login = createAction(`${prefix} LOGIN`, props<{username: string, password: string}>());
export const loginSuccess = createAction(`${prefix} LOGIN_SUCCESS`, props<{user: IUser}>());
export const loginError = createAction(`${prefix} LOGIN_ERROR`, props<{error}>());

export const logout = createAction(`${prefix} LOGOUT`);
export const logoutSuccess = createAction(`${prefix} LOGOUT_SUCCESS`);
export const logoutError = createAction(`${prefix} LOGOUT_ERROR`, props<{error}>());

export const removeError = createAction(`${prefix} REMOVE_ERROR`);
