import { createAction, props } from '@ngrx/store';

import { ILocation, IAllLocationsResponse, ILocationsPaginationRes } from './../../locations/locations.model';

const prefix = '[LOCATIONS]';

// ALL LOCATIONS
export const getAllLocations = createAction(`${prefix} GET_ALL_LOCATIONS`);
export const getAllLocationsSuccess = createAction(
    `${prefix} GET_ALL_LOCATIONS_SUCCESS`,
    props<{locations: IAllLocationsResponse}>()
);
export const getAllLocationsError = createAction(
    `${prefix} GET_ALL_LOCATIONS_ERROR`,
    props<{error}>()
);

// PAGINATED LOCATIONS
export const getPaginatedLocations = createAction(
    `${prefix} GET_PAGINATED_LOCATIONS`,
    props<{page: number}>()
);
export const getPaginatedLocationsSuccess = createAction(
    `${prefix} GET_PAGINATED_LOCATIONS_SUCCESS`,
    props<{data: ILocationsPaginationRes}>()
);
export const getPaginatedLocationsError = createAction(
    `${prefix} GET_PAGINATED_LOCATIONS_ERROR`,
    props<{error}>()
);

// SINGLE LOCATION
export const getLocation = createAction(
    `${prefix} GET_LOCATION`,
    props<{id: number}>()
);
export const getLocationSuccess = createAction(
    `${prefix} GET_LOCATION_SUCCESS`,
    props<{data: ILocation[]}>()
);
export const getLocationError = createAction(
    `${prefix} GET_LOCATION_ERROR`,
    props<{error}>()
);

// ADD LOCATION
export const addLocation = createAction(
    `${prefix} ADD_LOCATION`,
    props<{data: Partial<ILocation>}>()
);
export const addLocationSuccess = createAction(
    `${prefix} ADD_LOCATION_SUCCESS`,
    props<{data: ILocation}>()
);
export const addLocationError = createAction(
    `${prefix} ADD_LOCATION_ERROR`,
    props<{error}>()
);

// UPDATE LOCATION
export const updateLocation = createAction(
    `${prefix} UPDATE_LOCATION`,
    props<{id: number, name: string, location_id: number}>()
);
export const updateLocationSuccess = createAction(
    `${prefix} UPDATE_LOCATION_SUCCESS`,
    props<{data: ILocation}>()
);
export const updateLocationError = createAction(
    `${prefix} UPDATE_LOCATION_ERROR`,
    props<{error}>()
);

// DELETE LOCATION
export const deleteLocation = createAction(
    `${prefix} DELETE_LOCATION`,
    props<{id: number}>()
    );
export const deleteLocationSuccess = createAction(
    `${prefix} DELETE_LOCATION_SUCCESS`,
    props<{id: number}>()
);
export const deleteLocationError = createAction(
    `${prefix} DELETE_LOCATION_ERROR`,
    props<{error}>()
);

// CHANGE PAGE

export const changeLocationsPage = createAction(
    `${prefix} CHANGE_PAGE`,
    props<{page: number}>()
);
