import { createAction, props } from '@ngrx/store';

import { IAllEventsResponse, IEventsPaginationRes, IEvent } from './../../events/events.model';

const prefix = '[EVENTS]';

// ALL EVENTS
export const getAllEvents = createAction(`${prefix} GET_ALL_EVENTS`);
export const getAllEventsSuccess = createAction(
    `${prefix} GET_ALL_EVENTS_SUCCESS`,
    props<{events: IAllEventsResponse}>()
);
export const getAllEventsError = createAction(
    `${prefix} GET_ALL_EVENTS_ERROR`,
    props<{error}>()
);

// PAGINATED EVENTS
export const getPaginatedEvents = createAction(
    `${prefix} GET_PAGINATED_EVENTS`,
    props<{page: number}>()
);
export const getPaginatedEventsSuccess = createAction(
    `${prefix} GET_PAGINATED_EVENTS_SUCCESS`,
    props<{data: IEventsPaginationRes}>()
);
export const getPaginatedEventsError = createAction(
    `${prefix} GET_PAGINATED_EVENTS_ERROR`,
    props<{error}>()
);

// SINGLE EVENT
export const getEvent = createAction(
    `${prefix} GET_EVENT`,
    props<{id: number}>()
);
export const getEventSuccess = createAction(
    `${prefix} GET_EVENT_SUCCESS`,
    props<{event: IEvent}>()
);
export const getEventError = createAction(
    `${prefix} GET_EVENT_ERROR`,
    props<{error}>()
);

// ADD EVENT
export const addEvent = createAction(
    `${prefix} ADD_EVENT`,
    props<{name: string, location_id: number, company_id: number}>()
);
export const addEventSuccess = createAction(
    `${prefix} ADD_EVENT_SUCCESS`,
    props<{data: IEvent}>()
);
export const addEventError = createAction(
    `${prefix} ADD_EVENT_ERROR`,
    props<{error}>()
);

// UPDATE EVENT
export const updateEvent = createAction(
    `${prefix} UPDATE_EVENT`,
    props<{id: number, data: {name: string, location_id: number, company_id: number}}>()
);
export const updateEventSuccess = createAction(
    `${prefix} UPDATE_EVENT_SUCCESS`,
    props<{data: IEvent}>()
);
export const updateEventError = createAction(
    `${prefix} UPDATE_EVENT_ERROR`,
    props<{error}>()
);

// DELETE EVENT
export const deleteEvent = createAction(
    `${prefix} DELETE_EVENT`,
    props<{id: number}>()
);
export const deleteEventSuccess = createAction(
    `${prefix} DELETE_EVENT_SUCCESS`,
    props<{id: number}>()
);
export const deleteEventError = createAction(
    `${prefix} DELETE_EVENT_ERROR`,
    props<{error}>()
);

// CHANGE PAGE

export const changeEventsPage = createAction(
    `${prefix} CHANGE_PAGE`,
    props<{page: number}>()
);
