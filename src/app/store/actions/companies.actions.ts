import { createAction, props } from '@ngrx/store';

import { IAllCompaniesResponse, ICompaniesPaginationRes, ICompany } from './../../companies/companies.model';

const prefix = '[COMPANIES]';

// ALL COMPANIES
export const getAllCompanies = createAction(`${prefix} GET_ALL_COMPANIES`);
export const getAllCompaniesSuccess = createAction(
    `${prefix} GET_ALL_COMPANIES_SUCCESS`,
    props<{companies: IAllCompaniesResponse}>()
);
export const getAllCompaniesError = createAction(
    `${prefix} GET_ALL_COMPANIES_ERROR`,
    props<{error}>()
);

// PAGINATED COMPANIES
export const getPaginatedCompanies = createAction(
    `${prefix} GET_PAGINATED_COMPANIES`,
    props<{page: number}>()
);
export const getPaginatedCompaniesSuccess = createAction(
    `${prefix} GET_PAGINATED_COMPANIES_SUCCESS`,
    props<{data: ICompaniesPaginationRes}>()
);
export const getPaginatedCompaniesError = createAction(
    `${prefix} GET_PAGINATED_COMPANIES_ERROR`,
    props<{error}>()
);

// SINGLE COMPANY
export const getCompany = createAction(
    `${prefix} GET_COMPANY`,
    props<{id: number}>()
);
export const getCompanySuccess = createAction(
    `${prefix} GET_COMPANY_SUCCESS`,
    props<{company: ICompany}>()
);
export const getCompanyError = createAction(
    `${prefix} GET_COMPANY_ERROR`,
    props<{error}>()
);

// ADD COMPANY
export const addCompany = createAction(
    `${prefix} ADD_COMPANY`,
    props<{name: string, location_id: number}>()
);
export const addCompanySuccess = createAction(
    `${prefix} ADD_COMPANY_SUCCESS`,
    props<{company: ICompany}>()
);
export const addCompanyError = createAction(
    `${prefix} ADD_COMPANY_ERROR`,
    props<{error}>()
);

// UPDATE COMPANY
export const updateCompany = createAction(
    `${prefix} UPDATE_COMPANY`,
    props<{id: number, data: {name: string, location_id: number}}>()
);
export const updateCompanySuccess = createAction(
    `${prefix} UPDATE_COMPANY_SUCCESS`,
    props<{company: ICompany}>()
);
export const updateCompanyError = createAction(
    `${prefix} UPDATE_COMPANY_ERROR`,
    props<{error}>()
);

// DELETE COMPANY
export const deleteCompany = createAction(
    `${prefix} DELETE_COMPANY`,
    props<{id: number}>()
);
export const deleteCompanySuccess = createAction(
    `${prefix} DELETE_COMPANY_SUCCESS`,
    props<{id: number}>()
);
export const deleteCompanyError = createAction(
    `${prefix} DELETE_COMPANY_ERROR`,
    props<{error}>()
);

// CHANGE PAGE

export const changeCompaniesPage = createAction(
    `${prefix} CHANGE_PAGE`,
    props<{page: number}>()
);
