import { createAction } from '@ngrx/store';

const prefix = '[REQUESTING]';

export const startRequesting = createAction(`${prefix} START_REQUESTING`);
export const stopRequesting = createAction(`${prefix} STOP_REQUESTING`);
