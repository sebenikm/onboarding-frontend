import { IUser, IUserResponse } from './../../user.model';
import { createAction, props } from '@ngrx/store';

const prefix = '[USERS]';

// GET USERS
export const getUsers = createAction(`${prefix} GET_USERS`);

export const getUsersSuccess = createAction(
    `${prefix} GET_USERS_SUCCESS`,
    props<{users: IUser[]}>()
);

export const getUsersError = createAction(
    `${prefix} GET_USERS_ERROR`,
    props<{error}>()
);

// CHANGE ROLES
export const changeUserRole = createAction(
    `${prefix} CHANGE_USER_ROLE`,
    props<{user: IUser}>()
);

// DISCARD ROLE CHANGES
export const discardUsersRoleChanges = createAction(`${prefix} DISCARD_USERS_ROLE_CHANGES`);

// SAVE ROLE CHANGES
export const saveUsersRoleChanges = createAction(`${prefix} SAVE_USERS_ROLE_CHANGES`);

export const saveUsersRoleChangesSuccess = createAction(
    `${prefix} SAVE_USERS_ROLE_CHANGES_SUCCESS`
);

export const saveUsersRoleChangesError = createAction(
    `${prefix} SAVE_USERS_ROLE_CHANGES_ERROR`,
    props<{error}>()
);
