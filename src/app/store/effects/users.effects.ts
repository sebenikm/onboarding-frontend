import { saveUsersRoleChangesError } from './../actions/users.actions';
import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { catchError, switchMap, map, filter, withLatestFrom, tap } from 'rxjs/operators';

import { UsersService } from 'src/app/admin/users/services/users.service';
import { UsersActions } from '../actions';
import { State, selectUsers, selectUsersRoleChanges } from '../reducers';
import { ToasterService } from 'src/app/services/toaster.service';

@Injectable()
export class UsersEffects {

    getUsers$ = createEffect(() => this._actions$.pipe(
        ofType(UsersActions.getUsers),
        withLatestFrom(this._store.pipe(select(selectUsers))),
        filter(([action, users]) => !users.length),
        switchMap(() => this._usersService.getUsers().pipe(
            map(({data}) => UsersActions.getUsersSuccess(data)),
            catchError(error => of(UsersActions.getUsersError({error})))
        ))
    ));

    getUsersError$ = createEffect(() => this._actions$.pipe(
        ofType(UsersActions.getUsersError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Fetching users failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    updateUsersRoles$ = createEffect(() => this._actions$.pipe(
        ofType(UsersActions.saveUsersRoleChanges),
        withLatestFrom(this._store.pipe(select(selectUsersRoleChanges))),
        switchMap(([action, roles]) => this._usersService.updateRoles({data: roles}).pipe(
            map(() => UsersActions.saveUsersRoleChangesSuccess()),
            catchError(error => of(UsersActions.saveUsersRoleChangesError({error})))
        ))
    ));

    updateUsersRolesSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(UsersActions.saveUsersRoleChangesSuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Users roles successfuly updated.`, type: 'success'});
        })
    ), { dispatch: false });

    updateUsersRolesError$ = createEffect(() => this._actions$.pipe(
        ofType(UsersActions.saveUsersRoleChangesError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Updating users roles failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    constructor(
        private _actions$: Actions,
        private _usersService: UsersService,
        private _store: Store<State>,
        private _toasterService: ToasterService,
    ) {}

}
