import { State, selectEvents, selectPaginatedEvents } from './../reducers';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { select, Store } from '@ngrx/store';
import { createEffect, ofType, Actions } from '@ngrx/effects';

import { of } from 'rxjs';
import { switchMap, map, catchError, tap, withLatestFrom, filter, mergeMap, first } from 'rxjs/operators';

import { EventsActions } from '../actions';
import { EventsService } from './../../events/events.service';
import { ToasterService } from 'src/app/services/toaster.service';

@Injectable()
export class EventsEffects {

    getEvents$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.getAllEvents),
        withLatestFrom(this._store.pipe(select(selectEvents))),
        filter(([action, events]) => !events.length),
        switchMap(() => this._eventsService.getEvents().pipe(
            map(events => EventsActions.getAllEventsSuccess({events})),
            catchError(error => of(EventsActions.getAllEventsError(error)))
        ))
    ));

    getPaginatedEvents$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.getPaginatedEvents),
        mergeMap(({page}) =>
            this._store.pipe(
                select(selectPaginatedEvents(page)),
                first(),
                map(events => ({page, events}))
            )
        ),
        // filter(events => !events),
        switchMap(({events, page}) => {
            if (events) {
                return of(EventsActions.changeEventsPage({page}));
            }
            return this._eventsService.getPaginatedEvents(page).pipe(
                map(data => EventsActions.getPaginatedEventsSuccess({data})),
                catchError(error => of(EventsActions.getPaginatedEventsError(error)))
            );
        })
    ));

    getEventsError$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.getAllEventsError, EventsActions.getPaginatedEventsError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Fetching events failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    getEvent$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.getEvent),
        switchMap(({id}) => this._eventsService.getEvent(id).pipe(
            map(event => EventsActions.getEventSuccess({event})),
            catchError(error => of(EventsActions.getEventError(error)))
        ))
    ));

    getEventError$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.getEventError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Fetching event failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    addEvent$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.addEvent),
        switchMap(data => this._eventsService.addEvent(data).pipe(
            map(event => EventsActions.addEventSuccess(event)),
            catchError(error => of(EventsActions.addEventError(error)))
        ))
    ));

    addEventSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.updateEventSuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Event successfuly added.`, type: 'success'});
        })
    ), { dispatch: false });

    addEventError$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.updateEventError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Adding event failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    updateEvent$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.updateEvent),
        switchMap(({id, data}) => this._eventsService.updateEvent(id, data).pipe(
            map(event => EventsActions.updateEventSuccess(event)),
            catchError(error => of(EventsActions.updateEventError(error)))
        ))
    ));

    updateEventSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.updateEventSuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Event successfuly updated.`, type: 'success'});
        })
    ), { dispatch: false });

    updateEventError$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.updateEventError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Updating event failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    deleteEvent$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.deleteEvent),
        switchMap(({id}) => this._eventsService.deleteEvent(id).pipe(
            map(data => EventsActions.deleteEventSuccess(data)),
            catchError(error => of(EventsActions.deleteEventError(error)))
        ))
    ));

    deleteEventSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.deleteEventSuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Event successfuly deleted.`, type: 'success'});
            this._router.navigate(['events']);
        })
        ), { dispatch: false });

    deleteEventError$ = createEffect(() => this._actions$.pipe(
        ofType(EventsActions.deleteEventError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Deleting event failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    constructor(
        private _actions$: Actions,
        private _router: Router,
        private _eventsService: EventsService,
        private _store: Store<State>,
        private _toasterService: ToasterService,
    ) {}

}
