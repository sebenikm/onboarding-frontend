import { State, selectCompanies, selectPaginatedCompanies } from 'src/app/store/reducers';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { CompaniesService } from './../../companies/companies.service';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { switchMap, map, catchError, tap, withLatestFrom, filter, mergeMap, first } from 'rxjs/operators';
import { CompaniesActions } from '../actions';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { ToasterService } from 'src/app/services/toaster.service';

@Injectable()
export class CompaniesEffects {

    getCompanies$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.getAllCompanies),
        withLatestFrom(this._store.pipe(select(selectCompanies))),
        filter(([action, companies]) => !companies.length),
        switchMap(() => this._companiesService.getCompanies().pipe(
            map(companies => CompaniesActions.getAllCompaniesSuccess({companies})),
            catchError(error => of(CompaniesActions.getAllCompaniesError(error)))
        ))
    ));

    getPaginatedCompanies$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.getPaginatedCompanies),
        mergeMap(({page}) =>
            this._store.pipe(
                select(selectPaginatedCompanies(page)),
                first(),
                map(companies => ({page, companies}))
            )
        ),
        // filter((companies) => !companies),
        switchMap(({companies, page}) => {
            if (companies) {
                return of(CompaniesActions.changeCompaniesPage({page}));
            }
            return this._companiesService.getPaginatedCompanies(page).pipe(
                map(data => CompaniesActions.getPaginatedCompaniesSuccess({data})),
                catchError(error => of(CompaniesActions.getPaginatedCompaniesError(error)))
            );
        })
    ));

    getCompaniesError$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.getPaginatedCompaniesError, CompaniesActions.getAllCompaniesError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Fetching companies failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    getCompany$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.getCompany),
        switchMap(({id}) => this._companiesService.getCompany(id).pipe(
            map(company => CompaniesActions.getCompanySuccess({company})),
            catchError(error => of(CompaniesActions.getCompanyError(error)))
        ))
    ));

    getCompanyError$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.getCompanyError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Fetching company failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    addCompany$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.addCompany),
        switchMap(data => this._companiesService.addCompany(data).pipe(
            map(({data: company}) => CompaniesActions.addCompanySuccess({company})),
            catchError(error => of(CompaniesActions.addCompanyError(error)))
        ))
    ));

    addCompanySuccess$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.addCompanySuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Company successfuly added.`, type: 'success'});
        })
    ), { dispatch: false });

    addCompanyError$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.addCompanyError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Adding company failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    updateCompany$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.updateCompany),
        switchMap(({id, data}) => this._companiesService.updateCompany(id, data).pipe(
            map(({data: company}) => CompaniesActions.updateCompanySuccess({company})),
            catchError(error => of(CompaniesActions.updateCompanyError(error)))
        ))
    ));

    updateCompanySuccess$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.updateCompanySuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Company successfuly updated.`, type: 'success'});
        })
    ), { dispatch: false });

    updateCompanyError$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.updateCompanyError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Updating company failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    deleteCompany$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.deleteCompany),
        switchMap(({id}) => this._companiesService.deleteCompany(id).pipe(
            map(data => CompaniesActions.deleteCompanySuccess(data)),
            catchError(error => of(CompaniesActions.deleteCompanyError(error)))
        ))
    ));

    deleteCompanySuccess$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.deleteCompanySuccess),
        tap(() => {
            this._toasterService.showToaster({message: 'Company successfuly deleted.', type: 'success'});
            this._router.navigate(['companies']);
        })
    ), { dispatch: false });

    deleteCompanyError$ = createEffect(() => this._actions$.pipe(
        ofType(CompaniesActions.updateCompanyError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Deleting company failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    constructor(
        private _actions$: Actions,
        private _router: Router,
        private _companiesService: CompaniesService,
        private _store: Store<State>,
        private _toasterService: ToasterService,
    ) {}

}
