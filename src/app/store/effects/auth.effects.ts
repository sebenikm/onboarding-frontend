import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';

import { UserService } from './../../user.service';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthActions } from '../actions';
import { ToasterService } from 'src/app/services/toaster.service';

@Injectable()
export class AuthEffects {

    loggin$ = createEffect(() => this._actions$.pipe(
        ofType(AuthActions.login),
        switchMap((data) => this._userService.login(data).pipe(
            map(user => AuthActions.loginSuccess({user})),
            catchError(error => of(AuthActions.loginError({error})))
        ))
    ));

    loginSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(AuthActions.loginSuccess),
        tap(() => {
            this._toasterService.showToaster({message: 'Login successful', type: 'success'});
            this._router.navigate(['']);
        })
    ), { dispatch: false });

    loginError$ = createEffect(() => this._actions$.pipe(
        ofType(AuthActions.loginError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Login failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    constructor(
        private _actions$: Actions,
        private _router: Router,
        private _userService: UserService,
        private _toasterService: ToasterService,
    ) {}

}
