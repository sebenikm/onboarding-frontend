import { State, selectPaginatedLocations } from 'src/app/store/reducers';
import { Injectable } from '@angular/core';

import { createEffect, ofType, Actions } from '@ngrx/effects';
import { switchMap, map, catchError, tap, withLatestFrom, filter, mergeMap, first } from 'rxjs/operators';

import { LocationsActions } from '../actions';

import { LocationsService } from './../../locations/locations.service';
import { of, EMPTY } from 'rxjs';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { selectLocations } from '../reducers';
import { ToasterService } from 'src/app/services/toaster.service';

@Injectable()
export class LocationsEffects {

    getLocations$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.getAllLocations),
        withLatestFrom(this._store.pipe(select(selectLocations))),
        filter(([action, locations]) => !locations.length),
        switchMap(() => this._locationsService.getLocations().pipe(
            map(locations => LocationsActions.getAllLocationsSuccess({locations})),
            catchError(error => of(LocationsActions.getAllLocationsError(error)))
        ))
    ));

    getPaginatedLocations$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.getPaginatedLocations),
        mergeMap(({page}) =>
            this._store.pipe(
                select(selectPaginatedLocations(page)),
                first(),
                map(locations => ({page, locations}))
            )
        ),
        switchMap(({page, locations}) => {
            if (locations) {
                return of(LocationsActions.changeLocationsPage({page}));
            }
            return this._locationsService.getPaginatedLocations(page).pipe(
                map(data => LocationsActions.getPaginatedLocationsSuccess({data})),
                catchError(error => of(LocationsActions.getPaginatedLocationsError(error)))
            );
        })
    ));

    getLocationsError$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.getAllLocationsError, LocationsActions.getPaginatedLocationsError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Fetching locations failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    getLocation$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.getLocation),
        switchMap(({id}) => this._locationsService.getLocation(id).pipe(
            map(data => LocationsActions.getLocationSuccess({data})),
            catchError(error => of(LocationsActions.getLocationError(error)))
        ))
    ));

    getLocationError$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.getLocationError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Fetching location failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    addLocation$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.addLocation),
        switchMap(({data}) => this._locationsService.addLocation(data).pipe(
            map(location => LocationsActions.addLocationSuccess(location)),
            catchError(error => of(LocationsActions.addLocationError(error)))
        ))
    ));

    addLocationSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.addLocationSuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Location successfuly added.`, type: 'success'});
        })
    ), { dispatch: false });

    addLocationError$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.addLocationError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Adding location failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    updateLocation$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.updateLocation),
        switchMap(data => this._locationsService.updateLocation(data).pipe(
            map(location => LocationsActions.updateLocationSuccess(location)),
            catchError(error => of(LocationsActions.updateLocationError(error)))
        ))
    ));

    updateLocationSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.updateLocationSuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Location successfuly updated.`, type: 'success'});
        })
    ), { dispatch: false });

    updateLocationError$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.updateLocationError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Updating location failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    deleteLocation$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.deleteLocation),
        switchMap(({id}) => this._locationsService.deleteLocation(id).pipe(
            map(data => LocationsActions.deleteLocationSuccess(data)),
            catchError(error => of(LocationsActions.deleteLocationError(error)))
        ))
    ));

    deleteLocationSuccess$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.deleteLocationSuccess),
        tap(() => {
            this._toasterService.showToaster({message: `Location successfuly deleted.`, type: 'success'});
            this._router.navigate(['locations']);
        })
    ), { dispatch: false });

    deleteLocationError$ = createEffect(() => this._actions$.pipe(
        ofType(LocationsActions.deleteLocationError),
        tap(({error}) => {
            this._toasterService.showToaster({message: `Deleting location failed. ${error}`, type: 'error'});
        })
    ), { dispatch: false });

    constructor(
        private _actions$: Actions,
        private _router: Router,
        private _locationsService: LocationsService,
        private _store: Store<State>,
        private _toasterService: ToasterService,
    ) {}

}
