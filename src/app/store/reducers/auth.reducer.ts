import { IUser } from './../../user.model';
import { createReducer, on } from '@ngrx/store';

import { AuthActions } from '../actions';

export const featureKey = 'auth';

export interface State {
    user: IUser;
    error: string;
}

export const initialState: State = {
    user: null,
    error: null,
};

const _reducer = createReducer(initialState,
    on(AuthActions.loginSuccess , (state, {user}) => ({...state, user})),
    on(AuthActions.logoutSuccess , state => ({...state, user: null})),
    on(AuthActions.loginError, (state, {error}) => ({...state, error})),
    on(AuthActions.removeError, state => ({...state, error: null}))
);

// export for AOT
export function reducer(state, action) {
  return _reducer(state, action);
}

export const getUser = (state: State) => state.user;

export const getError = (state: State) => state.error;
