import { createReducer, on } from '@ngrx/store';

import { CompaniesActions } from '../actions';
import { ICompany } from './../../companies/companies.model';

export const featureKey = 'companies';

export interface State {
    companies: ICompany[];
    paginatedCompanies: {[key: string]: ICompany[]};
    company: ICompany;
    currentPage: number;
    lastPage: number;
    perPage: number;
    total: number;
    requesting: boolean;
}

export const initialState: State = {
    companies: [],
    paginatedCompanies: {},
    company: null,
    currentPage: 1,
    lastPage: 1,
    perPage: 1,
    total: 0,
    requesting: false,
};

const _reducer = createReducer(initialState,
    on(CompaniesActions.getAllCompaniesSuccess, (state, {companies}) => {
        return ({...state, companies: companies.data.companies});
    }),
    on(CompaniesActions.getPaginatedCompaniesSuccess, (state, {data}) => {
        const lastPage = data.meta.last_page;
        const currentPage = data.meta.current_page;
        const perPage = data.meta.per_page;
        const total = data.meta.total;
        const paginatedCompanies = {...state.paginatedCompanies, [currentPage]: data.data.companies};
        return ({...state, lastPage, currentPage, perPage, total, paginatedCompanies});
    }),
    on(CompaniesActions.getCompanySuccess, (state, {company}) => {
        return ({...state, company});
    }),
    on(CompaniesActions.addCompanySuccess, (state, {company}) => {
        const lastPage = state.total % state.perPage ? state.lastPage : state.lastPage + 1;
        const companies = state.companies.slice();
        const paginatedCompanies = {...state.paginatedCompanies};
        companies.push(company);
        if (state.currentPage === lastPage) {
            paginatedCompanies[lastPage].push(company);
        }
        const total = companies.length;
        return ({...state, lastPage, companies, total, paginatedCompanies});
    }),
    on(CompaniesActions.updateCompanySuccess, (state, {company}) => {
        const companies = state.companies.slice();
        const companyIndex = state.companies.findIndex(c => c.id === company.id);
        companies[companyIndex] = company;
        const paginatedCompanies = {...state.paginatedCompanies};
        for (const c of Object.values(paginatedCompanies)) {
            const cIndex = c.findIndex(_ => _.id === company.id);
            if (cIndex !== -1) {
                c[cIndex] = company;
                break;
            }
        }
        company = state.company && state.company.id === company.id ? company : state.company;
        return ({...state, companies, paginatedCompanies, company});
    }),
    on(CompaniesActions.deleteCompanySuccess, (state, {id}) => {
        const companies = state.companies.filter(c => c.id === id);
        const paginatedCompanies = {...state.paginatedCompanies};
        // TODO: There is a bug, because we would have to shift all other enteries by 1
        for (const c of Object.values(paginatedCompanies)) {
            const cIndex = c.findIndex(_ => _.id === id);
            if (cIndex !== -1) {
                c.splice(cIndex, 1);
                break;
            }
        }
        const company = state.company && state.company.id === id ? null : state.company;
        const total = state.total - 1;
        const lastPage = Math.floor(state.total / state.perPage) + 1 < state.lastPage ? state.lastPage - 1 : state.lastPage;
        const currentPage = state.currentPage > lastPage ? lastPage : state.currentPage;
        return ({...state, companies, paginatedCompanies, company, total, lastPage, currentPage});
    }),
    on(CompaniesActions.changeCompaniesPage, (state, {page}) => ({...state, currentPage: page})),
);

// export for AOT
export function reducer(state, action) {
  return _reducer(state, action);
}

export const getCompanies = (state: State) => state.companies;

export const getPaginatedCompanies = (state: State) => state.paginatedCompanies;

export const getCompany = (state: State) => state.company;

export const getCurrentPage = (state: State) => state.currentPage;

export const getLastPage = (state: State) => state.lastPage;

export const getRequesting = (state: State) => state.requesting;
