import { createReducer, on } from '@ngrx/store';

import { ILocation } from './../../locations/locations.model';
import { LocationsActions } from '../actions';

export const featureKey = 'locations';

export interface State {
    locations: ILocation[];
    paginatedLocations: {[key: string]: ILocation[]};
    location: ILocation;
    currentPage: number;
    lastPage: number;
    perPage: number;
    total: number;
    requesting: boolean;
}

export const initialState: State = {
    locations: [],
    paginatedLocations: {},
    location: null,
    currentPage: 1,
    lastPage: 1,
    perPage: 1,
    total: 0,
    requesting: false,
};

const _reducer = createReducer(initialState,
    on(LocationsActions.getAllLocationsSuccess, (state, {locations}) => {
        return ({...state, locations: locations.data.locations});
    }),
    on(LocationsActions.getPaginatedLocationsSuccess, (state, {data}) => {
        const lastPage = data.meta.last_page;
        const currentPage = data.meta.current_page;
        const perPage = data.meta.per_page;
        const total = data.meta.total;
        const locations = data.data.locations;
        const paginatedLocations = {...state.paginatedLocations, [currentPage]: data.data.locations};
        return ({...state, lastPage, currentPage, perPage, total, locations, paginatedLocations});
    }),
    on(LocationsActions.getLocationSuccess, (state, {data}) => {
        return ({...state, location: data[0]});
    }),
    on(LocationsActions.addLocationSuccess, (state, {data}) => {
        const lastPage = state.total % state.perPage ? state.lastPage : state.lastPage + 1;
        const locations = state.locations.slice();
        const paginatedLocations = {...state.paginatedLocations};
        locations.push(data);
        if (state.currentPage === lastPage) {
            paginatedLocations[lastPage].push(data);
        }
        const total = locations.length;
        return ({...state, lastPage, locations, total, paginatedLocations});
    }),
    on(LocationsActions.updateLocationSuccess, (state, {data}) => {
        const locations = state.locations.slice();
        const eventIndex = state.locations.findIndex(e => e.id === data.id);
        locations[eventIndex] = data;
        const paginatedLocations = {...state.paginatedLocations};
        for (const l of Object.values(paginatedLocations)) {
            const lIndex = l.findIndex(_ => _.id === data.id);
            if (lIndex !== -1) {
                l[lIndex] = data;
                break;
            }
        }
        const location = state.location && state.location.id === data.id ? data : state.location;
        return ({...state, locations, location, paginatedLocations});
    }),
    on(LocationsActions.deleteLocationSuccess, (state, {id}) => {
        const locations = state.locations.filter(l => l.id === id);
        const paginatedLocations = {...state.paginatedLocations};
        // TODO: There is a bug, because we would have to shift all other enteries by 1
        for (const l of Object.values(paginatedLocations)) {
            const lIndex = l.findIndex(_ => _.id === id);
            if (lIndex !== -1) {
                l.splice(lIndex, 1);
                break;
            }
        }
        const location = state.location && state.location.id === id ? null : state.location;
        const total = state.total - 1;
        const lastPage = Math.floor(state.total / state.perPage) + 1 < state.lastPage ? state.lastPage - 1 : state.lastPage;
        const currentPage = state.currentPage > lastPage ? lastPage : state.currentPage;
        return ({...state, locations, location, total, lastPage, currentPage, paginatedLocations});
    }),
    on(LocationsActions.changeLocationsPage, (state, {page}) => ({...state, currentPage: page})),
);

// export for AOT
export function reducer(state, action) {
  return _reducer(state, action);
}

export const getLocations = (state: State) => state.locations;

export const getPaginatedLocations = (state: State) => state.paginatedLocations;

export const getLocation = (state: State) => state.location;

export const getCurrentPage = (state: State) => state.currentPage;

export const getLastPage = (state: State) => state.lastPage;
