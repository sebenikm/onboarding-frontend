import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromLocations from './locations.reducer';
import * as fromEvents from './events.reducer';
import * as fromCompanies from './companies.reducer';
import * as fromAuth from './auth.reducer';
import * as fromRequesting from './requesting.reducer';
import * as fromUsers from './users.reducer';

export interface State {
    [fromLocations.featureKey]: fromLocations.State;
    [fromEvents.featureKey]: fromEvents.State;
    [fromCompanies.featureKey]: fromCompanies.State;
    [fromAuth.featureKey]: fromAuth.State;
    [fromRequesting.featureKey]: fromRequesting.State;
    [fromUsers.featureKey]: fromUsers.State;
}

export const initialState: State = {
  [fromLocations.featureKey]: fromLocations.initialState,
  [fromEvents.featureKey]: fromEvents.initialState,
  [fromCompanies.featureKey]: fromCompanies.initialState,
  [fromAuth.featureKey]: fromAuth.initialState,
  [fromRequesting.featureKey]: fromRequesting.initialState,
  [fromUsers.featureKey]: fromUsers.initialState,
};

export const reducers: ActionReducerMap<State> = {
  [fromLocations.featureKey]: fromLocations.reducer,
  [fromEvents.featureKey]: fromEvents.reducer,
  [fromCompanies.featureKey]: fromCompanies.reducer,
  [fromAuth.featureKey]: fromAuth.reducer,
  [fromRequesting.featureKey]: fromRequesting.reducer,
  [fromUsers.featureKey]: fromUsers.reducer,
};

// Locations selectors

export const selectLocationsFeature = createFeatureSelector<State, fromLocations.State>(fromLocations.featureKey);

export const selectLocations = createSelector(
    selectLocationsFeature,
    fromLocations.getLocations
);

const _selectPaginatedLocations = createSelector(
  selectLocationsFeature,
  fromLocations.getPaginatedLocations
);

export const selectPaginatedLocations = (page: number) => createSelector(
  _selectPaginatedLocations,
  (paginatedLocations) => paginatedLocations[page]
);

export const selectLocation = createSelector(
    selectLocationsFeature,
    fromLocations.getLocation
);

export const selectLocationsCurrentPage = createSelector(
  selectLocationsFeature,
  fromLocations.getCurrentPage
);

export const selectLocationsLastPage = createSelector(
  selectLocationsFeature,
  fromLocations.getLastPage
);

// Events selectors

export const selectEventsFeature = createFeatureSelector<State, fromEvents.State>(fromEvents.featureKey);

export const selectEvents = createSelector(
    selectEventsFeature,
    fromEvents.getEvents
);

const _selectPaginatedEvents = createSelector(
  selectEventsFeature,
  fromEvents.getPaginatedEvents
);

export const selectPaginatedEvents = (page: number) => createSelector(
  _selectPaginatedEvents,
  (paginatedEvents) => paginatedEvents[page]
);

export const selectEvent = createSelector(
    selectEventsFeature,
    fromEvents.getEvent
);

export const selectEventsCurrentPage = createSelector(
  selectEventsFeature,
  fromEvents.getCurrentPage
);

export const selectEventsLastPage = createSelector(
  selectEventsFeature,
  fromEvents.getLastPage
);

// Companies selectors

export const selectCompaniesFeature = createFeatureSelector<State, fromCompanies.State>(fromCompanies.featureKey);

export const selectCompanies = createSelector(
    selectCompaniesFeature,
    fromCompanies.getCompanies
);

const _selectPaginatedCompanies = createSelector(
  selectCompaniesFeature,
  fromCompanies.getPaginatedCompanies
);

export const selectPaginatedCompanies = (page: number) => createSelector(
  _selectPaginatedCompanies,
  (paginatedCompanies) => paginatedCompanies[page]
);

export const selectCompany = createSelector(
    selectCompaniesFeature,
    fromCompanies.getCompany
);

export const selectCompaniesCurrentPage = createSelector(
  selectCompaniesFeature,
  fromCompanies.getCurrentPage
);

export const selectCompaniesLastPage = createSelector(
  selectCompaniesFeature,
  fromCompanies.getLastPage
);

// Requesting selectors

export const selectRequestingFeature = createFeatureSelector<State, fromRequesting.State>(fromRequesting.featureKey);

export const selectRequesting = createSelector(
    selectRequestingFeature,
    fromRequesting.getRequesting
);

// Auth selectors

export const selectAuthFeature = createFeatureSelector<State, fromAuth.State>(fromAuth.featureKey);

export const selectUser = createSelector(
    selectAuthFeature,
    fromAuth.getUser
);

export const selectError = createSelector(
  selectAuthFeature,
  fromAuth.getError
);

// Users selectors

export const selectUsersFeature = createFeatureSelector<State, fromUsers.State>(fromUsers.featureKey);

export const selectUsers = createSelector(
  selectUsersFeature,
  fromUsers.getUsers
);

export const selectUsersRoleChanges = createSelector(
  selectUsersFeature,
  fromUsers.getUsersRoleChanges
);
