import { createReducer, on } from '@ngrx/store';

import { IUser } from '../../user.model';
import { UsersActions } from '../actions';
import { ERole } from 'src/app/admin/models/roles';

export const featureKey = 'users';

export interface State {
    users: IUser[];
    roleChanges: {
        id: number,
        admin: number
    }[];
}

export const initialState: State = {
    users: [],
    roleChanges: [],
};

const _reducer = createReducer(initialState,
    on(UsersActions.getUsersSuccess, (state, {users}) => ({...state, users})),
    on(UsersActions.changeUserRole, (state, {user}) => {
        const users = state.users.slice();
        const updatedUser = users.find(u => u.id === user.id);
        updatedUser.admin = updatedUser.admin === ERole.ADMIN ? ERole.DEVELOPER : ERole.ADMIN;
        const roleChanges = state.roleChanges.slice();
        const change = roleChanges.findIndex(u => u.id === user.id);
        if (change === -1) {
            roleChanges.push({id: updatedUser.id, admin: updatedUser.admin });
        } else {
            roleChanges.splice(change, 1);
        }
        return ({...state, users, roleChanges});
    }),
    on(UsersActions.saveUsersRoleChangesSuccess, (state) => {
        const roleChanges = [] as IUser[];
        return ({...state, roleChanges});
    }),
    on(UsersActions.discardUsersRoleChanges, (state) => {
        const users = state.users.slice();
        for (const u of state.roleChanges) {
            const user = users.find(_ => _.id === u.id);
            user.admin = user.admin === ERole.ADMIN ? ERole.DEVELOPER : ERole.ADMIN;
        }
        return ({...state, users, roleChanges: []});
    }),
);


export function reducer(state, action) {
    return _reducer(state, action);
  }

export const getUsers = (state: State) => state.users;

export const getUsersRoleChanges = (state: State) => state.roleChanges;
