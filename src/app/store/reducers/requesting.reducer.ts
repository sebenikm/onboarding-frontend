import { createReducer, on } from '@ngrx/store';

import { RequestingActions } from '../actions';

export const featureKey = 'requesting';

export interface State {
    requesting: boolean;
}

export const initialState: State = {
    requesting: false,
};

const _reducer = createReducer(initialState,
    on(RequestingActions.startRequesting, state => ({requesting: true})),
    on(RequestingActions.stopRequesting, stte => ({requesting: false}))
);

// export for AOT
export function reducer(state, action) {
  return _reducer(state, action);
}

export const getRequesting = (state: State) => state.requesting;
