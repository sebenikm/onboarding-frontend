import { createReducer, on } from '@ngrx/store';

import { EventsActions } from '../actions';
import { IEvent } from './../../events/events.model';

export const featureKey = 'events';

export interface State {
    events: IEvent[];
    paginatedEvents: {[key: string]: IEvent[]};
    event: IEvent;
    currentPage: number;
    lastPage: number;
    perPage: number;
    total: number;
    requesting: boolean;
}

export const initialState: State = {
    events: [],
    paginatedEvents: {},
    event: null,
    currentPage: 1,
    lastPage: 1,
    perPage: 1,
    total: 0,
    requesting: false,
};

const _reducer = createReducer(initialState,
    on(EventsActions.getAllEventsSuccess, (state, {events}) => {
        return ({...state, events: events.data.events});
    }),
    on(EventsActions.getPaginatedEventsSuccess, (state, {data}) => {
        const lastPage = data.meta.last_page;
        const currentPage = data.meta.current_page;
        const perPage = data.meta.per_page;
        const total = data.meta.total;
        const events = data.data.events;
        const paginatedEvents = {...state.paginatedEvents, [currentPage]: data.data.events};
        return ({...state, lastPage, currentPage, perPage, total, events, paginatedEvents});
    }),
    on(EventsActions.getEventSuccess, (state, {event}) => {
        return ({...state, event: event[0]});
    }),
    on(EventsActions.addEventSuccess, (state, {data}) => {
        const lastPage = state.total % state.perPage ? state.lastPage : state.lastPage + 1;
        const events = state.events.slice();
        const paginatedEvents = {...state.paginatedEvents};
        events.push(data);
        if (state.currentPage === lastPage) {
            paginatedEvents[lastPage].push(data);
        }
        const total = events.length;
        return ({...state, lastPage, events, total, paginatedEvents});
    }),
    on(EventsActions.updateEventSuccess, (state, {data}) => {
        const events = state.events.slice();
        const eventIndex = state.events.findIndex(e => e.id === data.id);
        events[eventIndex] = data;
        const paginatedEvents = {...state.paginatedEvents};
        for (const e of Object.values(paginatedEvents)) {
            const eIndex = e.findIndex(_ => _.id === data.id);
            if (eIndex !== -1) {
                e[eIndex] = data;
                break;
            }
        }
        const event = state.event && state.event.id === data.id ? data : state.event;
        return ({...state, events, event, paginatedEvents});
    }),
    on(EventsActions.deleteEventSuccess, (state, {id}) => {
        const events = state.events.filter(l => l.id === id);
        const paginatedEvents = {...state.paginatedEvents};
        // TODO: There is a bug, because we would have to shift all other enteries by 1
        for (const e of Object.values(paginatedEvents)) {
            const eIndex = e.findIndex(_ => _.id === id);
            if (eIndex !== -1) {
                e.splice(eIndex, 1);
                break;
            }
        }
        const event = state.event && state.event.id === id ? null : state.event;
        const total = state.total - 1;
        const lastPage = Math.floor(state.total / state.perPage) + 1 < state.lastPage ? state.lastPage - 1 : state.lastPage;
        const currentPage = state.currentPage > lastPage ? lastPage : state.currentPage;
        return ({...state, events, event, total, lastPage, currentPage, paginatedEvents});
    }),
    on(EventsActions.changeEventsPage, (state, {page}) => ({...state, currentPage: page})),
);

// export for AOT
export function reducer(state, action) {
  return _reducer(state, action);
}

export const getEvents = (state: State) => state.events;

export const getPaginatedEvents = (state: State) => state.paginatedEvents;

export const getEvent = (state: State) => state.event;

export const getCurrentPage = (state: State) => state.currentPage;

export const getLastPage = (state: State) => state.lastPage;

export const getRequesting = (state: State) => state.requesting;
