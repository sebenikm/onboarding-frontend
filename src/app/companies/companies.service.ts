import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';
import { IAllCompaniesResponse, ICompany, ICompaniesPaginationRes } from './companies.model';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {
  private _url = environment.apiUrl + 'companies';

  constructor(private _http: HttpClient) { }

  getCompanies(): Observable<IAllCompaniesResponse> {
    return this._http.get<IAllCompaniesResponse>(`${this._url}/all`);
  }

  getPaginatedCompanies(page: number = 1): Observable<ICompaniesPaginationRes> {
    const params = new HttpParams().append('page', `${page}`);
    return this._http.get<ICompaniesPaginationRes>(`${this._url}`, { params });
  }

  getCompany(id: number): Observable<ICompany> {
    return this._http.get<ICompany>(`${this._url}/${id}`);
  }

  addCompany(data: {name: string, location_id: number}): Observable<{data: ICompany}> {
    return this._http.post<{data: ICompany}>(`${this._url}`, data);
  }

  updateCompany(id: number, data: {name: string, location_id: number}): Observable<{data: ICompany}> {
    return this._http.put<{data: ICompany}>(`${this._url}/${id}`, data);
  }

  deleteCompany(id: number): Observable<any> {
    return this._http.delete<any>(`${this._url}/${id}`);
  }

}
