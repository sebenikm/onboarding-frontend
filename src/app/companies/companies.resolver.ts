import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { State, selectPaginatedCompanies } from './../store/reducers';
import { ICompany } from './companies.model';
import { Observable } from 'rxjs';
import { first, filter } from 'rxjs/operators';
import { CompaniesActions } from '../store/actions';

@Injectable({
    providedIn: 'root'
})
export class CompaniesResolver implements Resolve<ICompany[]> {

    constructor(private _store: Store<State>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICompany[]> {
        this._store.dispatch(CompaniesActions.getPaginatedCompanies({page: 1}));
        return this._store.pipe(
            select(selectPaginatedCompanies(1)),
            filter(companies => !!(companies && companies.length)),
            first(),
        );
    }

}
