import { CompanyResolver } from './company.resolver';
import { CompaniesResolver } from './companies.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompaniesComponent } from './components/companies/companies.component';
import { CompanyDetailsComponent } from './components/company-details/company-details.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
  { path: '', component: CompaniesComponent, canActivate: [AuthGuard], resolve: { CompaniesResolver } },
  { path: ':id', component: CompanyDetailsComponent, canActivate: [AuthGuard] , resolve: { CompanyResolver } },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class CompaniesRoutingModule { }
