import { ILocation } from './../locations/locations.model';
import { IPaginationResponse } from '../shared/shared.models';

export interface ICompany {
    id: number;
    name: string;
    location: ILocation;
}

export interface IAllCompaniesResponse {
    data: {
      companies: ICompany[],
    };
}

export interface ICompaniesPaginationRes extends IPaginationResponse {
  data: {
    companies: ICompany[],
  };
}
