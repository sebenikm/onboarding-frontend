import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

import { CompaniesActions } from '../store/actions';
import { ICompany } from './companies.model';
import { State, selectCompany } from 'src/app/store/reducers';
import { filter, first } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CompanyResolver implements Resolve<ICompany> {

    constructor(private _store: Store<State>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICompany> {
        // tslint:disable-next-line: no-string-literal
        const id = route.params['id'];
        this._store.dispatch(CompaniesActions.getCompany({id}));
        return this._store.pipe(
            select(selectCompany),
            filter(company => !!company),
            first(),
        );

    }

}