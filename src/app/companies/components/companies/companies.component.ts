import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store, select } from '@ngrx/store';

import { Observable, Subject } from 'rxjs';

import { CompaniesActions, LocationsActions } from 'src/app/store/actions';
import {
  State,
  selectLocations,
  selectCompaniesLastPage,
  selectCompaniesCurrentPage,
  selectPaginatedCompanies } from 'src/app/store/reducers';
import { ILocation } from '../../../locations/locations.model';
import { ICompany } from '../../companies.model';
import { switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit, OnDestroy {
  public companies: ICompany[] = [];
  public locations$: Observable<ILocation[]> = this._store.pipe(select(selectLocations));
  public currentPage$: Observable<number> = this._store.pipe(select(selectCompaniesCurrentPage));
  public lastPage$: Observable<number> = this._store.pipe(select(selectCompaniesLastPage));

  private _destroy$ = new Subject<void>();

  constructor(
    private _store: Store<State>
  ) { }

  ngOnInit(): void {
    this.getLocations();
    this.currentPage$.pipe(
      switchMap(page => this._store.pipe(select(selectPaginatedCompanies(page)))),
      takeUntil(this._destroy$),
    ).subscribe(companies => {
      this.companies = companies;
    });
  }

  public getAllCompanies(): void {
    this._store.dispatch(CompaniesActions.getAllCompanies());
  }

  public getPaginatedCompanies(page: number = 1): void {
    this._store.dispatch(CompaniesActions.getPaginatedCompanies({page}));
  }

  public onPageChange(page: number): void {
    this.getPaginatedCompanies(page);
  }

  public getLocations(): void {
    this._store.dispatch(LocationsActions.getAllLocations());
  }

  public onAddCompany(data: {name: string, location_id: number}): void {
    this._store.dispatch(CompaniesActions.addCompany(data));
  }

  public trackByFn(index: number, company: ICompany): number {
    return company.id;
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

}
