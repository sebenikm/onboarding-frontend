import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

import { select, Store } from '@ngrx/store';

import { Observable } from 'rxjs';
import { tap, filter } from 'rxjs/operators';

import { ILocation } from './../../../locations/locations.model';
import { ICompany } from './../../companies.model';
import { selectCompany, selectLocations, State } from 'src/app/store/reducers';
import { LocationsActions, CompaniesActions } from 'src/app/store/actions';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss'],
})
export class CompanyDetailsComponent implements OnInit {
  public company$: Observable<ICompany> = this._store.pipe(
    select(selectCompany),
    filter(c => !!c),
    tap(company => this.updateForm(company))
  );
  public locations$: Observable<ILocation[]> = this._store.pipe(select(selectLocations));
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    location_id: new FormControl('', Validators.required),
  });
  private _companyId: number;
  // TODO: implement requesting
  requesting: boolean;

  constructor(
    private _store: Store<State>,
  ) { }

  ngOnInit(): void {
    this.getLocations();
  }

  private getLocations(): void {
    this._store.dispatch(LocationsActions.getAllLocations());
  }

  private updateForm(company: ICompany): void {
    this._companyId = company.id;
    this.form.setValue({
      name: company.name,
      location_id: company.location.id
    });
  }

  onSave(): void {
    if (!this.form.valid) {
      return;
    }
    this._store.dispatch(CompaniesActions.updateCompany({data: this.form.value, id: this._companyId}));
  }

  onDelete(): void {
    this._store.dispatch(CompaniesActions.deleteCompany({id: this._companyId}));
  }

}
