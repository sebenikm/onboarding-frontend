import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ILocation } from 'src/app/locations/locations.model';

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompanyFormComponent implements OnInit {
  @Input() locations: ILocation[] = [];
  @Output() addCompany = new EventEmitter<{name: string, location_id: number}>();
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    location_id: new FormControl('', Validators.required),
  });

  constructor() { }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    this.addCompany.emit(this.form.value);
    this.form.reset();
  }

}
