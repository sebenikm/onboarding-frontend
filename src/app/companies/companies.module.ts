import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from './../shared/shared.module';
import { CompaniesRoutingModule } from './companies-routing.module';
import { CompaniesComponent } from './components/companies/companies.component';
import { CompanyFormComponent } from './components/company-form/company-form.component';
import { CompanyDetailsComponent } from './components/company-details/company-details.component';


@NgModule({
  declarations: [
    CompaniesComponent,
    CompanyFormComponent,
    CompanyDetailsComponent
  ],
  imports: [
    SharedModule,
    ReactiveFormsModule,
    CompaniesRoutingModule
  ]
})
export class CompaniesModule { }
