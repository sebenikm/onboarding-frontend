import { UserService } from './user.service';
import { CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ERole } from './admin/models/roles';

@Injectable({
    providedIn: 'root'
})
export class AdminGuard implements CanActivate, CanLoad {

    constructor(private _userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return state.url === '/login' ? !this._userService.user : this._userService.user.admin === ERole.ADMIN;
    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        return route.path === 'login' ? !this._userService.user : this._userService.user.admin === ERole.ADMIN;
    }

}
