import { Injectable, ComponentRef, ComponentFactoryResolver, Injector, ApplicationRef, Inject, EmbeddedViewRef } from '@angular/core';
import { ToasterComponent } from '../components/toaster/toaster.component';
import { Subject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {
  private _toasters: ComponentRef<ToasterComponent>[] = [];
  private _onRemove = new Subject<void>();

  constructor(
    @Inject(DOCUMENT) protected _document: any,
    private _cfr: ComponentFactoryResolver,
    private _injector: Injector,
    private _appRef: ApplicationRef,
  ) {
    this._onRemove.subscribe(() => {
      if (this._toasters.length) {
        this._showToaster();
      }
    });
  }

  showToaster(data: {message: string, type: 'success' | 'error'}): void {
    this._createToaster(data);
  }

  private _createToaster(data: {message: string, type: 'success' | 'error'}): void {
    const factory = this._cfr.resolveComponentFactory(ToasterComponent);
    const toasterRef = factory.create(this._injector);
    toasterRef.instance.type = data.type;
    toasterRef.instance.message = data.message;
    this._toasters.push(toasterRef);
    toasterRef.instance.onHide$.subscribe(() => this._removeToaster());
    if (this._toasters.length === 1) {
      this._showToaster();
    }
  }

  private _showToaster(): void {
    const toasterRef = this._toasters[0];
    this._appRef.attachView(toasterRef.hostView);
    const domElement = (toasterRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    const target: HTMLElement = this._document.querySelector('body');
    target.appendChild(domElement);
    requestAnimationFrame(() => toasterRef.instance.show());
  }

  private _removeToaster(): void {
    const toasterRef = this._toasters.shift();
    this._appRef.detachView(toasterRef.hostView);
    toasterRef.destroy();
    this._onRemove.next();
  }

}
