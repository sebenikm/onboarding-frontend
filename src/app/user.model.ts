export interface IUser {
    id: number;
    username: string;
    token: string;
    admin: number;
}

export interface IUserResponse {
    data: {
        users: IUser[];
    };
}
