import { Component, OnInit, HostBinding, HostListener } from '@angular/core';
import { AnimationEvent, trigger, state, style, transition, animate } from '@angular/animations';

import { Subject } from 'rxjs';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.scss'],
  animations: [
    trigger('animation', [
      state('enter', style({
        opacity: 0,
        transform: 'translateY(100%)',
      })),
      state('leave', style({
        opacity: 0,
        transform: 'translateY(-100%)',
      })),
      state('show', style({
        opacity: 1,
        transform: 'translateY(0)',
      })),
      transition('enter => show, show => leave', [
        animate('200ms')
      ]),
    ])
  ],
})
export class ToasterComponent implements OnInit {
  @HostBinding('class') type: 'success' | 'error' = 'success';
  @HostBinding('@animation') get animationState() {
    return this._state;
  }

  message: string;
  private _state: 'show' | 'enter' | 'leave' = 'enter';
  private _onShowDone$ = new Subject<void>();
  onShow$ = this._onShowDone$.asObservable();
  private _onHideDone$ = new Subject<void>();
  onHide$ = this._onHideDone$.asObservable();
  private _displayTime = 2000;

  constructor() { }

  ngOnInit(): void {
  }

  @HostListener('@animation.done', ['$event'])
  animationDone(event: AnimationEvent): void {
    if (event.fromState === 'enter' && event.toState === 'show') {
      console.log('setting timeout');
      setTimeout(() => {
        this.hide();
      }, this._displayTime);
      this._onShowDone$.next();
      this._onShowDone$.complete();
    } else if (event.fromState === 'show' && event.toState === 'leave') {
      this._onHideDone$.next();
      this._onHideDone$.complete();
    }
  }

  @HostListener('click')
  onClick(): void {
    this.hide();
  }

  show(): void {
    this._state = 'show';
  }

  hide(): void {
    this._state = 'leave';
  }

}
