import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Router } from '@angular/router';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { State } from './store/reducers';
import { Store } from '@ngrx/store';
import { AuthActions } from './store/actions';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private _router: Router,
        private _store: Store<State>
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req).pipe(
            catchError(err => {
            if ([400, 401, 403].indexOf(err.status) !== -1) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                this._store.dispatch(AuthActions.logout());
                if (this._router.url !== '/login') {
                    this._router.navigate(['/login']);
                }
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }

}
