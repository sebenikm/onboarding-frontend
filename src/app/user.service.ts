import { environment } from './../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { IUser } from './user.model';
import { Store, select } from '@ngrx/store';
import { State, selectUser } from './store/reducers';
import { tap } from 'rxjs/operators';
import { AuthActions } from './store/actions';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _url = environment.apiUrl + 'login';
  private _user: IUser;

  constructor(private _http: HttpClient, private _store: Store<State>) {
    this._store.pipe(
      select(selectUser),
    ).subscribe(user => this._user = user);
   }

  public login(data: {password: string, username: string}): Observable<IUser> {
    return this._http.post<IUser>(this._url, data);
  }

  public logout(): void {
    this._store.dispatch(AuthActions.logout());
  }

  public get user(): IUser {
    return this._user;
  }

}
