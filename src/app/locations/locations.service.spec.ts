import { environment } from './../../environments/environment.prod';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { LocationsService } from './locations.service';
import { ILocation } from './locations.model';

const mockData: ILocation[] = [
  {
    id: 1,
    name: 'test',
    country: 'somewhere'
  },
];

const responseData = {
  data: {
    locations: mockData
  }
};

describe('LocationsService', () => {
  let locationsService: LocationsService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        LocationsService
      ]
    });
    locationsService = TestBed.inject(LocationsService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(locationsService).toBeTruthy();
  });

  it('getLocations should get all locations', () => {

    locationsService.getLocations().subscribe(locations => {
      expect(locations).toEqual(responseData, 'returned data is not correct');
    },
      () => fail('request should succed')
    );

    const req = httpTestingController.expectOne(`${environment.apiUrl}locations/all`);

    expect(req.request.method).toEqual('GET');

    req.flush(responseData);

  });

  it('getLocation should get a location', () => {

    locationsService.getLocation(1).subscribe(location => {
      expect(location[0]).toEqual(mockData[0], 'returned wrong location');
      expect(location.length).toEqual(1, 'only one location should be returned');
    },
      () => fail('request should succed')
    );
    const req = httpTestingController.expectOne(`${environment.apiUrl}locations/1`);

    expect(req.request.method).toEqual('GET');

    req.flush([mockData[0]]);

  });

  it('addLocation should add location', () => {
    const data = {
      id: 2,
      name: 'test2',
      country: 'somewhereelse'
    };

    locationsService.addLocation(data).subscribe(loc => {
      expect(loc).toEqual({data}, 'expected location');
    },
      () => fail('request should succed')
    );

    const req = httpTestingController.expectOne(`${environment.apiUrl}locations`);

    expect(req.request.method).toEqual('POST');

    req.flush({data});

  });

  it('should update location', () => {
    const data = {
      id: mockData[0].id,
      name: mockData[0].name,
      location_id: 12
    };

    locationsService.updateLocation(data).subscribe(loc => {
      expect(loc).toEqual({data: mockData[0]}, 'returned data is wrong');
    },
      () => fail('request should succed')
    );

    const req = httpTestingController.expectOne(`${environment.apiUrl}locations/1`);

    expect(req.request.method).toEqual('PUT');

    req.flush({data: mockData[0]});

  });

  it('deleteLocation should delete location', () => {

    locationsService.deleteLocation(mockData[0].id).subscribe(id => {
      expect(id).toEqual(mockData[0].id, 'wrong id returned');
    },
      () => fail('request should succed')
    );

    const req = httpTestingController.expectOne(`${environment.apiUrl}locations/1`);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockData[0].id);

  });

  afterEach(() => {
    httpTestingController.verify();
  });

});
