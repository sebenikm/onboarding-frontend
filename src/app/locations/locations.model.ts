import { IPaginationResponse } from '../shared/shared.models';

export interface ILocation {
    id: number;
    name: string;
    country: string;
    created_at?: string;
    updated_at?: string;
}

export interface IAllLocationsResponse {
    data: {
      locations: ILocation[],
    };
}

export interface ILocationsPaginationRes extends IPaginationResponse {
  data: {
    locations: ILocation[],
  };
}
