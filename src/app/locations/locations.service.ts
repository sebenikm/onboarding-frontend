import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';
import { IAllLocationsResponse, ILocation, ILocationsPaginationRes } from './locations.model';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {
  private _url = environment.apiUrl + 'locations';

  constructor(private _http: HttpClient) { }

  getLocations(): Observable<IAllLocationsResponse> {
    return this._http.get<IAllLocationsResponse>(`${this._url}/all`);
  }

  getPaginatedLocations(page: number = 1): Observable<ILocationsPaginationRes> {
    const params = new HttpParams().append('page', `${page}`);
    return this._http.get<ILocationsPaginationRes>(`${this._url}`, { params });
  }

  getLocation(id: number): Observable<ILocation[]> {
    return this._http.get<ILocation[]>(`${this._url}/${id}`);
  }

  addLocation(data: Partial<ILocation>): Observable<{data: ILocation}> {
    return this._http.post<{data: ILocation}>(`${this._url}`, data);
  }

  updateLocation(data: {id: number, name: string, location_id: number}): Observable<{data: ILocation}> {
    return this._http.put<any>(`${this._url}/${data.id}`, data);
  }

  deleteLocation(id: number): Observable<any> {
    return this._http.delete<any>(`${this._url}/${id}`);
  }

}
