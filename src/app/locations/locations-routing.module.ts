import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationsComponent } from './components/locations/locations.component';
import { LocationDetailsComponent } from './components/location-details/location-details.component';
import { AuthGuard } from '../auth.guard';
import { LocationResolver } from './location.resolver';
import { LocationsResolver } from './locations.resolver';

const routes: Routes = [
  { path: '', component: LocationsComponent, canActivate: [AuthGuard], resolve: { LocationsResolver } },
  { path: ':id', component: LocationDetailsComponent, canActivate: [AuthGuard], resolve: { LocationResolver } },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class LocationsRoutingModule { }
