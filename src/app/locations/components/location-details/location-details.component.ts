import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { tap, filter } from 'rxjs/operators';

import { ILocation } from '../../locations.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { State, selectLocation } from 'src/app/store/reducers';
import { Store, select } from '@ngrx/store';
import { LocationsActions } from 'src/app/store/actions';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.scss'],
})
export class LocationDetailsComponent implements OnInit {
  location$: Observable<ILocation> = this._store.pipe(
    select(selectLocation),
    filter(c => !!c),
    tap(location => this.updateForm(location))
  );
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
  });
  private _locationId: number;
  // TODO: Implement requesting
  requesting: boolean;

  constructor(
    private _store: Store<State>,
  ) { }

  ngOnInit(): void { }


  private updateForm(location: ILocation): void {
    this._locationId = location.id;
    this.form.setValue({
      name: location.name,
      country: location.country
    });
  }

  onSave(): void {
    if (!this.form.valid) {
      return;
    }
    this._store.dispatch(LocationsActions.updateLocation({id: this._locationId, ...this.form.value}));
  }

  onDelete(): void {
    this._store.dispatch(LocationsActions.deleteLocation({id: this._locationId}));
  }

}
