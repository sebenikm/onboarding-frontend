import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ILocation } from '../../locations.model';

@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationFormComponent implements OnInit {
  @Output() addLocation = new EventEmitter<Partial<ILocation>>();
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
  });

  constructor() { }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if (this.form.valid) {
      this.addLocation.emit(this.form.value);
      this.form.reset();
    }
  }

}
