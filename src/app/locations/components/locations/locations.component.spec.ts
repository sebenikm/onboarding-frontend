import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PaginationComponent } from './../../../shared/components/pagination/pagination.component';
import { LocationsService } from './../../locations.service';
import { async, ComponentFixture, TestBed, fakeAsync, flush } from '@angular/core/testing';

import { LocationsComponent } from './locations.component';
import { ILocation } from '../../locations.model';
import { of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { LocationFormComponent } from '../location-form/location-form.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

const mockData: ILocation[] = [
  {
    id: 1,
    name: 'test',
    country: 'somewhere'
  },
];

const mockLocation = {
  data: {
    id: 2,
    name: 'test2',
    country: 'somewhereelse'
  },
};

describe('LocationsComponent', () => {
  let component: LocationsComponent;
  let fixture: ComponentFixture<LocationsComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  let _locationsService;

  beforeEach(async(() => {

    const locationsServiceSpy = jasmine.createSpyObj('Location Service', ['getPaginatedLocations', 'addLocation']);

    TestBed.configureTestingModule({
      declarations: [ LocationsComponent, LocationFormComponent, PaginationComponent ],
      providers: [{
        provide: LocationsService,
        useValue: locationsServiceSpy
      }],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(LocationsComponent);
      component = fixture.componentInstance;
      de = fixture.debugElement;
      el = de.nativeElement;
      _locationsService = TestBed.inject(LocationsService);
      _locationsService.getPaginatedLocations.and.returnValue(of(
        {
          data: {
            locations: mockData
          },
          meta: {
            per_page: 10,
            current_page: 1,
            last_page: 1,
            total: 1
          }
        }
      ));
      _locationsService.addLocation.and.returnValue(of(mockLocation));
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get locations on init', () => {
    fixture.detectChanges();
    expect(_locationsService.getPaginatedLocations).toHaveBeenCalledTimes(1);
    expect(component.locations.length).toBe(mockData.length, 'wrong number of locations');
  });

  it('should display fetched locations', () => {
    fixture.detectChanges();
    const locations = de.queryAll(By.css('tr'));
    expect(locations.length).toBe(mockData.length + 1);
  });

  it('should add location if we are on last page and its not full', () => {
    fixture.detectChanges();
    const numLoc = component.locations.length;
    component.onAddLocation(mockLocation.data);
    fixture.detectChanges();
    expect(component.locations.length).toBe(numLoc + 1, 'wrong number of locations after adding location');
    expect(_locationsService.addLocation).toHaveBeenCalledTimes(1);
  });

  it('should display additional location if we are on last page and its not full', () => {
    fixture.detectChanges();
    component.onAddLocation(mockLocation.data);
    fixture.detectChanges();
    const locations = de.queryAll(By.css('tr'));
    expect(locations.length).toBe(component.locations.length + 1, 'wrong number of displaayed locations after adding location');
    expect(_locationsService.addLocation).toHaveBeenCalledTimes(1);
  });

  it('should not add location if we are not on last page or page was full', () => {
    _locationsService.getPaginatedLocations.and.returnValue(of(
      {
        data: {
          locations: mockData
        },
        meta: {
          per_page: 1,
          current_page: 1,
          last_page: 2,
          total: 2
        }
      }
    ));
    fixture.detectChanges();
    const numLoc = component.locations.length;
    component.onAddLocation(mockLocation.data);
    fixture.detectChanges();
    expect(component.locations.length).toBe(numLoc, 'wrong number of locations after adding location');
    expect(_locationsService.addLocation).toHaveBeenCalledTimes(1);
  });

  it('should call change page once and with correct arguments', () => {
    const page = 2;
    fixture.detectChanges();
    component.onPageChange(page);
    fixture.detectChanges();
    expect(_locationsService.getPaginatedLocations).toHaveBeenCalledTimes(2);
    expect(_locationsService.getPaginatedLocations).toHaveBeenCalledWith(page);
  });

});
