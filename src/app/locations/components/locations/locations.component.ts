import { Component, OnInit, OnDestroy } from '@angular/core';

import { ILocation } from '../../locations.model';
import { Store, select } from '@ngrx/store';
import {
  State,
  selectLocationsCurrentPage,
  selectLocationsLastPage,
  selectPaginatedLocations } from 'src/app/store/reducers';
import { LocationsActions } from 'src/app/store/actions';
import { Observable, Subject } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit, OnDestroy {
  public locations: ILocation[] = [];
  public currentPage$: Observable<number> = this._store.pipe(select(selectLocationsCurrentPage));
  public lastPage$: Observable<number> = this._store.pipe(select(selectLocationsLastPage));
  private _destroy$ = new Subject<void>();


  constructor(
    private _store: Store<State>
  ) { }

  ngOnInit(): void {
    this.currentPage$.pipe(
      switchMap(page => this._store.pipe(select(selectPaginatedLocations(page)))),
      takeUntil(this._destroy$),
    ).subscribe(locations => {
      this.locations = locations;
    });
  }

  public getAllLocations(): void {
    this._store.dispatch(LocationsActions.getAllLocations());
  }

  public getPaginatedLocations(page: number = 1): void {
    this._store.dispatch(LocationsActions.getPaginatedLocations({page}));
  }

  public onPageChange(page: number): void {
    this.getPaginatedLocations(page);
  }

  public onAddLocation(data: Partial<ILocation>): void {
    this._store.dispatch(LocationsActions.addLocation({data}));
  }

  public trackByFn(index: number, location: ILocation): number {
    return location.id;
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

}
