import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from './../shared/shared.module';
import { LocationsRoutingModule } from './locations-routing.module';
import { LocationsComponent } from './components/locations/locations.component';
import { LocationFormComponent } from './components/location-form/location-form.component';
import { LocationDetailsComponent } from './components/location-details/location-details.component';


@NgModule({
  declarations: [
    LocationsComponent,
    LocationFormComponent,
    LocationDetailsComponent
  ],
  imports: [
    SharedModule,
    ReactiveFormsModule,
    LocationsRoutingModule,
  ],
})
export class LocationsModule { }
