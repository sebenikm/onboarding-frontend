import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

import { LocationsActions } from '../store/actions';
import { State, selectLocation } from 'src/app/store/reducers';
import { filter, first } from 'rxjs/operators';
import { ILocation } from './locations.model';

@Injectable({
    providedIn: 'root'
})
export class LocationResolver implements Resolve<ILocation> {

    constructor(private _store: Store<State>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILocation> {
        // tslint:disable-next-line: no-string-literal
        const id = route.params['id'];
        this._store.dispatch(LocationsActions.getLocation({id}));
        return this._store.pipe(
            select(selectLocation),
            filter(location => !!location),
            first(),
        );

    }

}