import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { State, selectPaginatedLocations } from './../store/reducers';
import { Observable } from 'rxjs';
import { first, filter } from 'rxjs/operators';
import { LocationsActions } from '../store/actions';
import { ILocation } from './locations.model';

@Injectable({
    providedIn: 'root'
})
export class LocationsResolver implements Resolve<ILocation[]> {

    constructor(private _store: Store<State>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILocation[]> {
        this._store.dispatch(LocationsActions.getPaginatedLocations({page: 1}));
        return this._store.pipe(
            select(selectPaginatedLocations(1)),
            filter(locations => !!(locations && locations.length)),
            first(),
        );
    }

}
