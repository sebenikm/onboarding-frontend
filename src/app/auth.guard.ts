import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route, CanLoad } from '@angular/router';

import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

    constructor(private _userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return state.url === '/login' ? !this._userService.user : !!this._userService.user;
    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        return route.path === 'login' ? !this._userService.user : !!this._userService.user;
    }

}
