import { State, selectUser } from 'src/app/store/reducers';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';

import { IUser } from './../user.model';
import { Store, select } from '@ngrx/store';
import { ERole } from '../admin/models/roles';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {
  user: IUser;

  constructor(
    private _store: Store<State>,
    private _cdr: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this._store.pipe(select(selectUser)).subscribe(user => {
      this.user = user;
      this._cdr.detectChanges();
    });
  }

  public isAdmin(): boolean {
    return this.user.admin === ERole.ADMIN;
  }

}
