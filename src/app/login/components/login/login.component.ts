import { takeUntil, withLatestFrom, tap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthActions } from 'src/app/store/actions';
import { State, selectError } from 'src/app/store/reducers';
import { Store, select } from '@ngrx/store';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  requesting: boolean;
  error$ = this._store.pipe(
    select(selectError),
    tap(error => {
      if (error) {
        this.form.markAsPristine();
      }
    })
  );
  form = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });
  private _destroy$ = new Subject<void>();

  constructor(
    private _store: Store<State>,
  ) { }

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      withLatestFrom(this.error$),
      takeUntil(this._destroy$)
    ).subscribe(([change, error]) => {
      if (error) {
        this._store.dispatch(AuthActions.removeError());
      }
    });
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    this._store.dispatch(AuthActions.login({...this.form.value}));
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

}
