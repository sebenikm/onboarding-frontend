export interface IPaginationLinks {
    first: string;
    last: string;
    prev: string | null;
    next: string | null;
}

export interface IPaginationMeta {
    current_page: number;
    from: number;
    last_page: number;
    path: string;
    per_page: number;
    to: number;
    total: number;
}

export interface IPaginationResponse {
    links: IPaginationLinks;
    meta: IPaginationMeta;
}
