import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() currentPage = 1;
  @Input() lastPage = 1;
  @Input() range = 7;
  @Output() pageChange = new EventEmitter<number>();
  numbers: number[];

  constructor() { }

  ngOnInit(): void {
    this.setDisplayedRange();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((changes.currentPage && !changes.currentPage.firstChange)
    || (changes.lastPage && !changes.lastPage.firstChange)) {
      this.setDisplayedRange();
    }
  }

  private setDisplayedRange(): void {
    let start: number;
    let end: number;
    const mid = Math.floor(this.range / 2);
    if (this.currentPage - mid <= 0) {
      start = 1;
      end = Math.min(this.range, this.lastPage);
    } else {
      if (this.currentPage + mid >= this.lastPage) {
        start = Math.max(this.lastPage - this.range, 1);
        end = this.lastPage;
      } else {
        start = Math.max(this.currentPage - mid, 1);
        end = Math.min(start + this.range, this.lastPage);
      }
    }
    this.numbers = [];
    for (let i = start; i <= end; i++) {
      this.numbers.push(i);
    }
  }

  trackByFn(index: number, n: number): number {
    return n;
  }

  onFirst(): void {
    this.onPageChange(1);
  }

  onPrev(): void {
    this.onPageChange(this.currentPage - 1);
  }

  onClick(page: number): void {
    this.onPageChange(page);
  }

  onNext(): void {
    this.onPageChange(this.currentPage + 1);
  }

  onLast(): void {
    this.onPageChange(this.lastPage);
  }

  onPageChange(page: number) {
    this.pageChange.emit(page);
  }

}
