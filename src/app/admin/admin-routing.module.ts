import { UsersResolver } from './users/users.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './containers/admin.component';
import { AuthGuard } from '../auth.guard';
import { AdminGuard } from './../admin.guard';

const routes: Routes = [
  { path: '', component: AdminComponent, canActivate: [AuthGuard, AdminGuard], children: [
    {
      path: 'companies',
      loadChildren: () => import('../companies/companies.module').then(m => m.CompaniesModule),
      canLoad: [AuthGuard, AdminGuard],
    },
    {
      path: 'locations',
      loadChildren: () => import('../locations/locations.module').then(m => m.LocationsModule),
      canLoad: [AuthGuard, AdminGuard],
    },
    {
      path: 'events',
      loadChildren: () => import('../events/events.module').then(m => m.EventsModule),
      canLoad: [AuthGuard, AdminGuard],
    },
    {
      path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
      canLoad: [AuthGuard, AdminGuard],
    },
    { path: '', redirectTo: 'users', pathMatch: 'full' },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
