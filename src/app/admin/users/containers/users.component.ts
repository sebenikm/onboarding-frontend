import { ERole } from './../../models/roles';
import { State, selectUsers, selectUsersRoleChanges } from 'src/app/store/reducers';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IUser } from 'src/app/user.model';
import { UsersActions } from 'src/app/store/actions';
import { tap, map } from 'rxjs/operators';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users$ = this._store.pipe(
    select(selectUsers),
    map(users => users.filter(u => u.id !== this._currentUser.id))
  );
  changedRoles$ = this._store.pipe(select(selectUsersRoleChanges));
  private _ctaAdmin = 'upgrade to admin';
  private _ctaDeveloper = 'change to developer';
  private _currentUser: IUser;

  constructor(private _store: Store<State>, private _userService: UserService) { }

  ngOnInit(): void {
    this.getCurrentUser();
  }

  trackByFn(index: number, user: IUser): number {
    return user.id;
  }

  getCurrentUser(): void {
    this._currentUser = this._userService.user;
  }

  getCurrentRole(user: IUser): string {
    return user.admin === ERole.ADMIN ? 'Admin' : 'Developer';
  }

  changeRoleTo(user): string {
    return user.admin === ERole.ADMIN ? this._ctaDeveloper : this._ctaAdmin;
  }

  onChangeRole(user: IUser): void {
    this._store.dispatch(UsersActions.changeUserRole({user}));
  }

  onSaveChanges(): void {
    this._store.dispatch(UsersActions.saveUsersRoleChanges());
  }

  onDiscardChanges(): void {
    this._store.dispatch(UsersActions.discardUsersRoleChanges());
  }

}
