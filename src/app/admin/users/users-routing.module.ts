import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersResolver } from './users.resolver';
import { UsersComponent } from './containers/users.component';
import { UsersCanDeactivate } from './users.can-deactivate';

const routes: Routes = [{ path: '', component: UsersComponent, resolve: { UsersResolver }, canDeactivate: [UsersCanDeactivate] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
