import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';
import { filter, take, first, tap } from 'rxjs/operators';

import { State } from './../../store/reducers';
import { IUser } from './../../user.model';
import { UsersActions } from '../../store/actions';
import { selectUsers } from './../../store/reducers';

@Injectable({
    providedIn: 'root'
})
export class UsersResolver implements Resolve<IUser[]> {

    constructor(private _store: Store<State>) {}

    resolve(): Observable<IUser[]> {
        this._store.dispatch(UsersActions.getUsers());
        return this._store.pipe(
            select(selectUsers),
            filter(users => !!(users && users.length)),
            first()
        );
    }

}
