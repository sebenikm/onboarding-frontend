import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

import { take, map, tap } from 'rxjs/operators';
import { UsersComponent } from './containers/users.component';
import { State, selectUsersRoleChanges } from 'src/app/store/reducers';

@Injectable({
    providedIn: 'root'
})
export class UsersCanDeactivate implements CanDeactivate<UsersComponent> {

    constructor(private _store: Store<State>) {}

    canDeactivate(
        component: UsersComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState: RouterStateSnapshot): Observable<boolean> {
        return this._store.pipe(
            select(selectUsersRoleChanges),
            map(changes => !changes.length),
            take(1)
        );
    }

}