import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { IUser, IUserResponse } from 'src/app/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private _url = environment.apiUrl;

  constructor(private _http: HttpClient) { }

  getUsers(): Observable<IUserResponse> {
    return this._http.get<IUserResponse>(`${this._url}users`);
  }

  updateRoles(data: {data: {id: number, admin: number }[]}): Observable<any> {
    return this._http.put<any>(`${this._url}userStatus`, data);
  }

}
