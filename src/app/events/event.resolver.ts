import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

import { EventsActions } from '../store/actions';
import { State, selectEvent } from 'src/app/store/reducers';
import { filter, first } from 'rxjs/operators';
import { IEvent } from './events.model';

@Injectable({
    providedIn: 'root'
})
export class EventResolver implements Resolve<IEvent> {

    constructor(private _store: Store<State>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEvent> {
        // tslint:disable-next-line: no-string-literal
        const id = route.params['id'];
        this._store.dispatch(EventsActions.getEvent({id}));
        return this._store.pipe(
            select(selectEvent),
            filter(company => !!company),
            first(),
        );

    }

}