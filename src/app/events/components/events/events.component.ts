import { selectPaginatedEvents } from './../../../store/reducers/index';
import { State, selectLocations, selectEventsCurrentPage, selectEventsLastPage, selectCompanies } from 'src/app/store/reducers';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IEvent } from '../../events.model';
import { ICompany } from '../../../companies/companies.model';
import { ILocation } from '../../../locations/locations.model';
import { Store, select } from '@ngrx/store';
import { EventsActions, CompaniesActions, LocationsActions } from 'src/app/store/actions';
import { Observable, Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit, OnDestroy {
  public events: IEvent[] = [];
  public companies$: Observable<ICompany[]> = this._store.pipe(select(selectCompanies));
  public locations$: Observable<ILocation[]> = this._store.pipe(select(selectLocations));
  public currentPage$: Observable<number> = this._store.pipe(select(selectEventsCurrentPage));
  public lastPage$: Observable<number> = this._store.pipe(select(selectEventsLastPage));
  private _destroy$ = new Subject<void>();

  constructor(
    private _store: Store<State>
  ) { }

  ngOnInit(): void {
    this.getCompanies();
    this.getLocations();
    this.currentPage$.pipe(
      switchMap(page => this._store.pipe(select(selectPaginatedEvents(page)))),
      takeUntil(this._destroy$),
    ).subscribe(events => {
      this.events = events;
    });
  }

  public getAllEvents(): void {
    this._store.dispatch(EventsActions.getAllEvents());
  }

  public getPaginatedEvents(page: number = 1): void {
    this._store.dispatch(EventsActions.getPaginatedEvents({page}));
  }

  public onPageChange(page: number): void {
    this.getPaginatedEvents(page);
  }

  public getCompanies(): void {
    this._store.dispatch(CompaniesActions.getAllCompanies());
  }

  public getLocations(): void {
    this._store.dispatch(LocationsActions.getAllLocations());
  }

  public onAddEvent(data: {name: string, company_id: number, location_id: number}): void {
    this._store.dispatch(EventsActions.addEvent(data));
  }

  public trackByFn(index: number, event: IEvent): number {
    return event.id;
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

}
