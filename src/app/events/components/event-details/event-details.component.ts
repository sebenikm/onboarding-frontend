import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Observable } from 'rxjs';
import { tap, filter } from 'rxjs/operators';

import { ICompany } from './../../../companies/companies.model';
import { ILocation } from 'src/app/locations/locations.model';
import { IEvent } from '../../events.model';
import { Store, select } from '@ngrx/store';
import { State, selectCompanies, selectLocations, selectEvent } from 'src/app/store/reducers';
import { EventsActions, CompaniesActions, LocationsActions } from 'src/app/store/actions';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss'],
})
export class EventDetailsComponent implements OnInit {
  public event$: Observable<IEvent> = this._store.pipe(
    select(selectEvent),
    filter(e => !!e),
    tap(event => this.updateForm(event))
  );
  public companies$: Observable<ICompany[]> = this._store.pipe(select(selectCompanies));
  public locations$: Observable<ILocation[]> = this._store.pipe(select(selectLocations));
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    company_id: new FormControl('', Validators.required),
    location_id: new FormControl('', Validators.required),
  });
  private _eventId: number;
  // TODO: Implement requesting
  requesting: boolean;

  constructor(
    private _store: Store<State>,
  ) { }

  ngOnInit(): void {
    this.getLocations();
    this.getCompanies();
  }

  private getLocations(): void {
    this._store.dispatch(LocationsActions.getAllLocations());
  }

  public getCompanies(): void {
    this._store.dispatch(CompaniesActions.getAllCompanies());
  }

  private updateForm(event: IEvent): void {
    this._eventId = event.id;
    this.form.setValue({
      name: event.name,
      company_id: event.company.id,
      location_id: event.location.id
    });
  }

  onSave(): void {
    if (!this.form.valid) {
      return;
    }
    this._store.dispatch(EventsActions.updateEvent({id: this._eventId, data: this.form.value}));
  }

  onDelete(): void {
    this._store.dispatch(EventsActions.deleteEvent({id: this._eventId}));
  }

}
