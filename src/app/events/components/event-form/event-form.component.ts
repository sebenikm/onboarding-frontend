import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ICompany } from './../../../companies/companies.model';
import { ILocation } from 'src/app/locations/locations.model';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventFormComponent implements OnInit {
  @Input() companies: ICompany[] = [];
  @Input() locations: ILocation[] = [];
  @Output() addEvent = new EventEmitter<{name: string, company_id: number, location_id: number}>();
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    location_id: new FormControl('', Validators.required),
    company_id: new FormControl('', Validators.required),
  });

  constructor() { }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    this.addEvent.emit(this.form.value);
    this.form.reset();
  }

}
