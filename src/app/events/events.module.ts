import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from './../shared/shared.module';
import { EventsRoutingModule } from './events-routing.module';
import { EventsComponent } from './components/events/events.component';
import { EventFormComponent } from './components/event-form/event-form.component';
import { EventDetailsComponent } from './components/event-details/event-details.component';


@NgModule({
  declarations: [
    EventsComponent,
    EventFormComponent,
    EventDetailsComponent
  ],
  imports: [
    SharedModule,
    ReactiveFormsModule,
    EventsRoutingModule
  ]
})
export class EventsModule { }
