import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventsComponent } from './components/events/events.component';
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { AuthGuard } from '../auth.guard';
import { EventsResolver } from './events.resolver';
import { EventResolver } from './event.resolver';

const routes: Routes = [
  { path: '', component: EventsComponent, canActivate: [AuthGuard], resolve: { EventsResolver } },
  { path: ':id', component: EventDetailsComponent, canActivate: [AuthGuard], resolve: { EventResolver } },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class EventsRoutingModule { }
