import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { State, selectPaginatedEvents } from './../store/reducers';
import { Observable } from 'rxjs';
import { first, filter } from 'rxjs/operators';
import { EventsActions } from '../store/actions';
import { IEvent } from './events.model';

@Injectable({
    providedIn: 'root'
})
export class EventsResolver implements Resolve<IEvent[]> {

    constructor(private _store: Store<State>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEvent[]> {
        this._store.dispatch(EventsActions.getPaginatedEvents({page: 1}));
        return this._store.pipe(
            select(selectPaginatedEvents(1)),
            filter(events => !!(events && events.length)),
            first(),
        );
    }

}
