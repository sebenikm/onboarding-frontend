import { ICompany } from '../companies/companies.model';
import { ILocation } from '../locations/locations.model';
import { IPaginationResponse } from '../shared/shared.models';

export interface IEvent {
    id: number;
    name: string;
    location: ILocation;
    company: ICompany;
}

export interface IAllEventsResponse {
    data: {
      events: IEvent[],
    };
}

export interface IEventsPaginationRes extends IPaginationResponse {
  data: {
    events: IEvent[],
  };
}
