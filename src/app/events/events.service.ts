import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

import { IAllEventsResponse, IEvent, IEventsPaginationRes } from './events.model';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  private _url = environment.apiUrl + 'events';

  constructor(private _http: HttpClient) { }

  getEvents(): Observable<IAllEventsResponse> {
    return this._http.get<IAllEventsResponse>(`${this._url}/all`);
  }

  getPaginatedEvents(page: number = 1): Observable<IEventsPaginationRes> {
    const params = new HttpParams().append('page', `${page}`);
    return this._http.get<IEventsPaginationRes>(`${this._url}`, { params });
  }

  getEvent(id: number): Observable<IEvent> {
    return this._http.get<IEvent>(`${this._url}/${id}`);
  }

  updateEvent(id: number, data: {name: string, location_id: number, company_id: number}): Observable<{data: IEvent}> {
    return this._http.put<{data: IEvent}>(`${this._url}/${id}`, data);
  }

  deleteEvent(id: number): Observable<any> {
    return this._http.delete<any>(`${this._url}/${id}`);
  }

  addEvent(data: {name: string, location_id: number, company_id: number}): Observable<{data: IEvent}> {
    return this._http.post<{data: IEvent}>(`${this._url}`, data);
  }

}
